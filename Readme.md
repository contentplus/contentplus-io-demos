# Komponenty demonstracyjne dla platformy contentplus.io

## Testowanie komponentów:

W edytorze markdown wstaw kod:

    \widget-proposal={demo/vue-with-editor}

    \widget-proposal={demo/simple-editor}

    \widget-proposal={demo/app-messenger}

    \widget-proposal={demo/advanced-create-widget-slot}

Przełącz na tryb wizualny. Po kliknięciu na blok, otworzy się zaprogramowany edytor.
