import $ from 'jquery';

export default class {
    async init(container, api, options) {
        let inner = $('<div style="padding: 1rem; border: 1px solid red">');
        $(container).append(inner);
        this.module = await api.createWidgetSlot(inner[0], 'default', {
            inlineEditor: true
        }).catch((e) => {
            console.error(e);
            let placeholder = document.createElement('p');
            placeholder.append(document.createTextNode('Slot is empty'))
            inner.appendChild(placeholder)
        })
    }

    destroy() {
        if (this.module) {
            this.module.deinit();
        }
    }

    setState(stateData) {
        // ...
    }

    getState() {
        // ...
    }
};
