import Vue from "vue";
import Main from './Main';
import './style.css';

export default class {
    init(api) {
        api.addTab('basic', 'Edycja')
    }

    destroy() {

    }

    async initTab(tab, container, api) {
        this.app = new Vue({
            el: container,
            render(h) {
                return h(Main, {
                    props: {
                        editorMode: true,
                        api: api,
                        data: {},
                    }
                })
            },
            beforeDestroy() {
                this.$root.$el.parentNode.removeChild(this.$root.$el)
            },
        });

        await api.loadCss(
            api.enginePath('dist/entry.css')
        );
    }

    setState(stateData) {
        this.app.$children[0].setState(stateData)
    }

    getState() {
        return this.app.$children[0].getState()
    }

    destroyTab() {
        this.app.$destroy();
        this.app = null;
    }
};
