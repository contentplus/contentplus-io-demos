import Entry from "./entry";

export default class {
    init(api) {
        api.addTab('basic', 'Basic')
    }

    destroy() {

    }

    initTab(tab, container, api, data) {
        this.app = new Entry();
        this.app.init(container, api, {})
    }

    getDependencySlots() {
        return [
            'default'
        ];
    }

    destroyTab(tab, container, api) {
        this.app.destroy();
    }


    setState(stateData) {
        // ...
    }

    getState() {
        // ...
    }
};
