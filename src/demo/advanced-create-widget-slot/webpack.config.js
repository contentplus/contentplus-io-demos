const webpack = require("webpack");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

const VueLoader = require('vue-loader');
const TerserPlugin = require('terser-webpack-plugin');
const globImporter = require('node-sass-glob-importer');

let env = process.env.NODE_ENV === 'production' ? 'production' : 'development';
let isDev = env === 'development';

let webpackEntries = {
    'entry': './entry.js',
    'editorEntry': './editorEntry.js',
};

let port = 31536;

module.exports = {
    entry: webpackEntries,
    mode: env,
    devServer: {
        port: port,
        hot: true,
        writeToDisk: true,
        disableHostCheck: true,
        overlay: true
    },
    watch: isDev,
    module: {
        rules: [
            {
                test: /\.vue$/,
                use: [
                    {
                        loader: 'vue-loader',
                        options: {
                            compilerOptions: {
                                whitespace: 'condense'
                            }
                        }
                    }
                ]
            },
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                        plugins: ["@babel/plugin-transform-runtime"]
                    }
                }
            },
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            // you can specify a publicPath here
                            // by default it uses publicPath in webpackOptions.output
                            publicPath: './',
                            hmr: isDev,
                        },
                    },
                    'css-loader',
                    {
                        loader: 'resolve-url-loader'
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true,
                            sourceMapContents: false,
                            importer: globImporter()
                        }
                    }
                ]
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            // you can specify a publicPath here
                            // by default it uses publicPath in webpackOptions.output
                            publicPath: './',
                            hmr: isDev,
                        },
                    },
                    'css-loader'
                ]
            },
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                use: [
                    'file-loader',
                    {
                        loader: 'image-webpack-loader',
                        options: {
                            outputPath: './files/',
                            disable: false
                        },
                    },
                ],
            },
            {
                test: /\.(woff(2)?|ttf|eot)(\?.*)?$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: './files/',
                        }
                    }
                ]
            }
        ],
    },
    optimization: {
        minimize: !isDev,
        minimizer: [
            new TerserPlugin({
                cache: true,
                parallel: true,
                sourceMap: true, // Must be set to true if using source-maps in production
                terserOptions: {
                    // https://github.com/webpack-contrib/terser-webpack-plugin#terseroptions
                }
            }),
            new OptimizeCSSAssetsPlugin({})
        ],
    },
    output: {
        libraryTarget: 'amd'
    },
    externals: {
        vue: 'vue',
        jquery: 'jquery'
    },
    resolve: {
        extensions: ['*', '.js', '.vue', '.json']
    },
    plugins: [
        new VueLoader.VueLoaderPlugin(),
        new MiniCssExtractPlugin({
            filename: '[name].css'
        }),
        isDev ? new webpack.HotModuleReplacementPlugin() : null
    ].filter(v => v !== null)
};
