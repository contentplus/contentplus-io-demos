define(['jquery'], function ($) {
    return function () {
        return {
            init(container, api, options) {
                var container1 = $('<div>').append('<h1>engineData.json</h1>'),
                    container2 = $('<div>').append('<h1>innerData.json</h1>');

                $(container)
                    .append('<p>Loaded data:</p>')
                    .append(container1)
                    .append(container2);

                $.get(api.enginePath('engineData.json'))
                    .then(function (response) {
                        container1.append($('<pre>').text(JSON.stringify(response)))
                    })
                    .fail(function (err) {
                        container1.append($('<p>').text('Nie udało się załadować'))
                    });

                $.get(api.dataPath('innerData.json'))
                    .then(function (response) {
                        container2.append($('<pre>').text(JSON.stringify(response)))
                    })
                    .fail(function (err) {
                        container2.append($('<p>').text('Nie udało się załadować'))
                    });
            },
            destroy(container) {
                $(container).remove();
            }
        };
    };
});
