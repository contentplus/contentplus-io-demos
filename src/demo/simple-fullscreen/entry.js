define(['jquery'], function ($) {
    return function () {
        return {
            state: 0,
            frozen: false,
            api: null,
            container: null,
            init: function (container, api, options) {
                var fullscreenContainer = $('<div>');
                $(container).append(
                    $(fullscreenContainer)
                        .append(
                            $('<h1>').text('Fullscreen demo')
                        )
                        .append(
                            $('<button>')
                                .text('Leave fullscreen')
                                .on('click', function () {
                                    api.exitFullscreen();
                                })
                        )
                        .append(
                            $('<button>')
                                .text('Toggle partial fullscreen')
                                .on('click', function () {
                                    api.toggleFullscreen(fullscreenContainer[0]);
                                })
                        )
                );

                $(container)
                    .append(
                        $('<button>')
                            .text('Request fullscreen entire app')
                            .on('click', function () {
                                api.requestFullscreen();
                            })
                    )
                    .append(
                        $('<button>')
                            .text('Request partial fullscreen')
                            .on('click', function () {
                                api.requestFullscreen(fullscreenContainer[0]);
                            })
                    )
            },
            destroy: function (container) {
                $(container).remove();
            },
        };
    };
});
