import Vue from "vue";
import Main from './Main'

export default class {
    init(container, api, options) {
        this.api = api;
        this.app = new Vue({
            el: container,
            render(h) {
                return h('Main', {
                    props: {
                        api: api
                    }
                })
            },
            beforeDestroy() {
                this.$root.$el.parentNode.removeChild(this.$root.$el)
            },
            components: {Main}
        });
    }

    component() {
        return this.app.$children[0];
    }

    destroy() {
        this.app.$destroy();
    }

    isStateValid(state) {
        return this.component().isStateValid(state);
    }

    setState(stateData) {
        this.component().setState(stateData)
    }

    getState() {
        return this.component().getState();
    }

    setStateFrozen(isFrozen) {
        this.component().setStateFrozen(isFrozen);
    }
};
