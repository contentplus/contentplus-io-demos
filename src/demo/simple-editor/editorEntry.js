define(['jquery'], function ($) {
    return function () {
        return {
            init(api) {
                api.addTab('tab1', 'Tab1')
                api.addTab('tab2', 'Tab2')

                this.currentState = {
                    title: '',
                    description: '',
                };
            },
            destroy() {
            },
            initTab(tab, container, api, data) {

                this._tab = tab;

                let context = this;

                $(container).append(
                    $('<h1>').text(tab)
                )

                if (tab === 'tab1') {
                    this.input = $('<input type="text">')
                        .on('change', function () {
                            context.currentState.title = $(this).val();
                        });
                    $(container)
                        .append(
                            '<label>Title</label>'
                        )
                        .append(this.input)
                }
                if (tab === 'tab2') {
                    this.input = $('<input type="text">')
                        .on('change', function () {
                            context.currentState.description = $(this).val();
                        });
                    $(container)
                        .append(
                            '<label>Description</label>'
                        )
                        .append(this.input)
                }

                this.input.on('change', function () {
                    api.triggerStateSave();
                })

                return api.loadCss(api.enginePath('editor.css'));
            },
            destroyTab(tab, container, api) {
                $(container).html('')
            },
            setState(state) {
                state = state || {};

                this.currentState.title = state.title;
                this.currentState.description = state.description;

                this._interfaceRestoreCurrentState();
            },
            getState() {
                return this.currentState;
            },
            _interfaceRestoreCurrentState() {
                if (this._tab === 'tab1') {
                    this.input.val(this.currentState.title);
                }
                if (this._tab === 'tab2') {
                    this.input.val(this.currentState.description);
                }
            }
        };
    };
});
