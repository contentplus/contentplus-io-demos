define(['jquery'], function ($) {
    return function () {
        return {
            state: 0,
            frozen: false,
            api: null,
            container: null,
            init: function (container, api, options) {
                $(container)
                    .append($('<h1>').text(options.data.title))
                    .append($('<p>').text(options.data.description));

                return api.loadCss(
                    api.enginePath('widget.css')
                );
            },
            destroy: function (container) {
                $(container).remove();
            },
        };
    };
});
