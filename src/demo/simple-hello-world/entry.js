define(['jquery'], function ($) {
    return function () {
        return {
            init(container, api, options) {
                $(container)
                    .append('<p>Hello world!</p>')
                    .append(
                        $('<pre>').text(JSON.stringify(options, null, '\t'))
                    )
            },
            destroy(container) {
                $(container).remove();
            }
        };
    };
});
