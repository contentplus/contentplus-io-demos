export default class {
    async init(container, api, options, loader) {
        this.api = api;
        if (options.data.id) {
            if (!options.dependencies.includes(options.data.id)) {
                throw new Error('Missing dependency ' + options.data.id);
            }

            return this.api.embedWidget(container, options.data.id);
        }

        return this.api.embedWidget(container, options.data.manifest);
    }

    destroy() {
        this.api.destroy();
    }
};
