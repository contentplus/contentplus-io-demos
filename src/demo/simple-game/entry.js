define(['jquery'], function ($) {
    return function () {
        return {
            init: function (container, api, options) {
                $(container)
                    .append(
                        $('<button>').text('Wygrana').on('click', function () {
                            api.gameSuccessful();
                        })
                    )
                    .append(
                        $('<button>').text('Przegrana').on('click', function () {
                            api.gameFailed();
                        })
                    );
            },
            destroy: function (container) {
                $(container).remove();
            },
        };
    };
});
