define(['jquery'], function ($) {
    return function () {
        return {
            state: 0,
            frozen: false,
            api: null,
            container: null,
            init: function (container, api, options) {
                var fullscreenContainer = $('<div>');

                var img = $('<img alt="">').attr('src', api.enginePath('demo1.jpg'));

                img.on('click', function () {
                    api.openGallery([
                        img[0]
                    ]);
                });

                $(container).append(img)
            },
            destroy: function (container) {
                $(container).remove();
            },
        };
    };
});
