define(['jquery'], function ($) {
    return function () {
        return {
            counter: 0,
            frozen: false,
            api: null,
            container: null,
            init: function (container, api) {
                this.api = api;
                this.container = container;
                this.print();
            },
            print: function () {
                var context = this;
                $(this.container)
                    .html('')
                    .append('<p>Counter' + this.counter + '</p>')
                    .append(
                        $('<button>Increment</button>').on('click', function () {
                            if (context.frozen) {
                                return;
                            }

                            context.counter++;
                            context.api.triggerStateSave();
                            context.print();
                        })
                    )
            },
            destroy: function (container) {
                $(container).remove();
            },
            setState: function (stateData) {
                this.counter = parseInt(stateData) || 0;
                this.print();
            },
            getState: function () {
                return this.counter;
            },
            setStateFrozen: function (isFrozen) {
                this.frozen = isFrozen;
            }
        };
    };
});
