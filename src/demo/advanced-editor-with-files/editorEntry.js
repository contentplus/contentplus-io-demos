define(['jquery'], function ($) {
    return function () {
        return {
            init(api) {
                api.addTab('tab1', 'Tab1')
                api.addTab('tab2', 'Tab2')

                this.currentState = {
                    title: '',
                    howManyFiles: 0,
                };

                this.files = {};
            },
            destroy() {
            },
            initTab(tab, container, api, data) {

                this._tab = tab;

                let context = this;

                $(container).append(
                    $('<h1>').text(tab)
                ).append(
                    this.filesContainer = $('<div>')
                );

                this.api = api;

                if (tab === 'tab1') {
                    this.input = $('<input type="text">')
                        .on('change', function () {
                            context.currentState.title = $(this).val();
                        });
                    $(container)
                        .append(
                            '<label>Title</label>'
                        )
                        .append(this.input)
                }
                if (tab === 'tab2') {
                    this.input = $('<input type="number">')
                        .on('change', function () {
                            context.currentState.howManyFiles = parseInt($(this).val());
                        });
                    $(container)
                        .append(
                            '<label>How many files?</label>'
                        )
                        .append(this.input)
                }

                this.input.on('change', function () {
                    api.triggerStateSave();
                })

                return api.loadCss(api.enginePath('editor.css'));
            },
            setFiles(files) {
                console.log('Files has been changed', files);

                this.files = files;
                this._interfaceRestoreCurrentState();
            },
            getFiles(state) {
                var result = {
                    IMG_BACKGROUND: {
                        'label': 'Background image'
                    }
                };

                for (let i = 0; i < state.howManyFiles; i++) {
                    result['IMG_' + i] = {
                        'label': 'File ' + (i + 1)
                    };
                }

                return result;
            },
            destroyTab(tab, container, api) {
                $(container).html('')
            },
            setState(state) {
                state = state || {};

                this.currentState.title = state.title;
                this.currentState.howManyFiles = state.howManyFiles;

                this._interfaceRestoreCurrentState();
            },
            getState() {
                return this.currentState;
            },
            _interfaceRestoreCurrentState() {
                if (this._tab === 'tab1') {
                    this.input.val(this.currentState.title);
                }
                if (this._tab === 'tab2') {
                    this.input.val(this.currentState.howManyFiles);
                }

                if (this.filesContainer) {
                    this.filesContainer.html('');
                    for (let fileCode in this.files) {
                        let file = this.files[fileCode];

                        this.filesContainer.append(
                            $('<div>').text('Plik: ' + fileCode + ' znajduje się tutaj: ' + this.api.dataPath(file))
                        )
                    }
                }
            }
        };
    };
});
