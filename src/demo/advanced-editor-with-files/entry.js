define(['jquery'], function ($) {
    return function () {
        return {
            state: 0,
            frozen: false,
            api: null,
            container: null,
            init: function (container, api, options) {
                $(container)
                    .append($('<h1>').text(options.data.title))
                    .append($('<p>').text(options.data.description));

                if (options.files.IMG_BACKGROUND) {
                    $(container).css({
                        'background-image': 'url(' + api.dataPath(options.files.IMG_BACKGROUND) + ')'
                    })
                }

                for (let i = 0; i < options.data.howManyFiles; i++) {
                    console.warn(options.files['IMG_' + i])

                    $(container).append(
                        $('<p>')
                            .text('File path: ' +
                                (
                                    options.files['IMG_' + i] ?
                                        api.dataPath(options.files['IMG_' + i]) :
                                        'File has not been uploaded'
                                )
                            )
                    );
                }

                return api.loadCss(
                    api.enginePath('widget.css')
                );
            },
            destroy: function (container) {
                $(container).remove();
            },
        };
    };
});
