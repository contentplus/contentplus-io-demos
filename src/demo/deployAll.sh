#!/bin/bash
find . -maxdepth 1 -mindepth 1 -type d | while read line ; do
  (cd $line; make deploy)
done
