import Vue from "vue";
import Messenger from './Main';
import './style.css';

export default class {
    init(api) {
        this.files = {};

        api.addTab('basic', 'Edycja')
    }

    destroy() {

    }

    async initTab(tab, container, api) {
        this.app = new Vue({
            el: container,
            data() {
                return {
                    files: {}
                };
            },
            render(h) {
                return h('Messenger', {
                    props: {
                        editorMode: true,
                        api: api,
                        data: {},
                        files: this.files
                    }
                })
            },
            components: {Messenger},
            beforeDestroy() {
                this.$root.$el.parentNode.removeChild(this.$root.$el)
            },
        });

        this.app.files = this.files;

        await api.loadCss(
            api.enginePath('dist/entry.css')
        );
    }

    setState(stateData) {
        this.app.$children[0].setState(stateData)
    }

    getState() {
        return this.app.$children[0].getState()
    }

    setFiles(files) {
        this.files = files;
        if (this.app) {
            this.app.files = files;
        }
    }

    getFiles() {
        return {
            'LEFT': {
                'label': 'Mordka z lewej'
            },
            'RIGHT': {
                'label': 'Mordka z prawej'
            }
        };
    }

    destroyTab() {
        this.app.$destroy();
        this.app = null;
    }
};
