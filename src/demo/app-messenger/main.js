import Vue from "vue";
import Messenger from './Main';
import './style.css';

export default class {
    async init(container, api, options) {
        this.api = api;
        this.app = new Vue({
            el: container,
            render(h) {
                return h('Messenger', {
                    props: {
                        api: api,
                        data: options.data,
                        files: options.files || {},
                    }
                })
            },
            components: {Messenger},
            beforeDestroy() {
                this.$root.$el.parentNode.removeChild(this.$root.$el)
            },
        });
        await api.loadCss(
            api.enginePath('dist/entry.css')
        );
    }

    component() {
        return this.app.$children[0];
    }

    destroy() {
        this.app.$destroy();
    }

    isStateValid() {
        return this.component().isStateValid();
    }

    setState(stateData) {
        this.component().setState(stateData)
    }

    getState() {
        return this.component().getState();
    }

    setStateFrozen(isFrozen) {
        this.component().setStateFrozen(isFrozen);
    }
};
