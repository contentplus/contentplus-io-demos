define(['jquery'], function ($) {
    return function () {
        return {
            state: 0,
            frozen: false,
            api: null,
            container: null,
            init: function (container, api) {
                this.api = api;
                this.container = container;

                let context = this;

                $(this.container)
                    .append(
                        $('<input type="checkbox">').on('change', function () {
                            if (context.frozen) {
                                return;
                            }

                            context.state = $(this).is(':checked');
                            context.api.triggerStateSave();
                        })
                    )
            },
            isStateValid: function (state) {
                return state === true;
            },
            destroy: function (container) {
                $(container).remove();
            },
            setState: function (stateData) {
                this.state = stateData;
                $(this.container).find('input').prop('checked', stateData ? true : false);
            },
            getState: function () {
                return this.state;
            },
            setStateFrozen: function (isFrozen) {
                this.frozen = isFrozen;
                $(this.container).find('input').prop('disabled', isFrozen);
            }
        };
    };
});
